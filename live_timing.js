(function($) {

  var P = window.performance;
  if ( P && P.timing && P.timing.navigationStart ) {
    $(window).load(function() {
      setTimeout(function() {
        var data = Drupal.settings.live_timing.data;
        data.ready = (P.timing.domContentLoadedEventEnd - P.timing.navigationStart) / 1000;
        data.load = (P.timing.loadEventEnd - P.timing.navigationStart) / 1000;

        console.time && console.time('Save live timing');
        $.post(Drupal.settings.live_timing.script, data, function() {
          console.timeEnd && console.timeEnd('Save live timing');
        });
      }, 200);
    });
  }

})(jQuery);
