<?php

// Find Drupal root.
$root = __DIR__;
while (!file_exists($root . '/index.php')) {
  $root = realpath($root . '/..');
}

// Simple bootstrap.
define('DRUPAL_ROOT', $root);
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);

require_once __DIR__ . '/live_timing.inc';
_live_timing_save();
