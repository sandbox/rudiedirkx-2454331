<?php

/**
 * Save timing result.
 *
 * @see live_timing_save()
 * @see live_timing.php
 */
function _live_timing_save() {
  header('Content-type: text/plain; charset=utf-8');

  if (isset($_POST['theme'], $_POST['uri'], $_POST['ready'], $_POST['load'], $_POST['token'])) {
    $token_data = array(
      date('Y-m-d'),
      $_POST['theme'],
      $_POST['uri'],
      drupal_get_hash_salt(),
    );
    if ($_POST['token'] === md5(implode('-', $token_data))) {
      db_insert('live_timing')
        ->fields(array(
          'created' => time(),
          'theme' => (string) $_POST['theme'],
          'uri' => substr((string) $_POST['uri'], 0, 255),
          'onready' => (float) $_POST['ready'],
          'onload' => (float) $_POST['load'],
          'ip' => $_SERVER['REMOTE_ADDR'],
        ))
        ->execute();

      exit("OK\n");
    }
  }

  exit("NO\n");
}
